from dolfin import *


# Read XML mesh file and write XDMF
mesh = UnitSquareMesh(1, 1)
File("simple.xml") << mesh
File("simple.xdmf") << mesh

# Read XML mesh file and write XDMF
mesh = Mesh("gear.xml")
File("gear.xdmf") << mesh

mesh = refine(mesh)
mesh = refine(mesh)
mesh = refine(mesh)
File("gear-refined-big.xml") << mesh
